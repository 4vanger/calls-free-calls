var path = require('path');

exports.config = {
	// See docs at https://github.com/brunch/brunch/blob/master/docs/config.md
	modules: {
		definition: false,
		wrapper: false
	},
	paths: {
		"public": 'public',
		"watched": ['app', 'vendor', 'bower_components']
	},
	files: {
		javascripts: {
			joinTo: {
				'js/app.js': /^app/,
				'js/vendor.js': function(path) {
					path = path.replace(/\\/g, '/');
					switch(path) {
						case 'bower_components/modernizr/modernizr.js':
						case 'bower_components/jquery/dist/jquery.js':
						case 'bower_components/angular/angular.js':
						case 'bower_components/angular-route/angular-route.js':
						case 'bower_components/angular-animate/angular-animate.js':
							return true;
						default:
							return false;
					}
				},
				'test/scenarios.js': /^test(\/|\\)e2e/,
				'js/partials.js': /partials\.js/
			}
		},
		stylesheets: {
			joinTo: {
				'css/app.css': /^app/
			}
		},
		templates: {
			joinTo: {
				'js/partials.js': /^app\/partials\//
			}
		}
	},

	server: {
		path: 'jst-server.js'
	},

	conventions: {
		assets: /app(\\|\/)assets(\\|\/)/
	},
	plugins: {
		html2js: {
			options: {
				base: 'app/partials',
				htmlmin: {
					removeComments: true,
					collapseWhitespace: true
				}
			}
		}
	},

	sourceMaps: true
};
