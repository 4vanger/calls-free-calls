window.app.animation '.ad-content', ['$timeout', ($timeout) ->
	removeClass: (el, className, done) ->
		el = $(el)
		parent = el.closest('ul')
		# add some space on bottom to position everything correctly
		parent.css marginBottom: '500px'
		position = el.closest('li').offset()
		body = $(document.body)
		body.animate {scrollTop: position.top + 'px'}, 500, ->
			# ensure that element is positioned correctly
			# TODO replace this quick and dirty fix with something better
			position = el.closest('li').offset()
			scrollTop = body.scrollTop()
			if Math.abs(scrollTop - position.top) > 100
				body.animate {scrollTop: position.top + 'px'}, 100
			parent.css marginBottom: 'auto'
		return
]
