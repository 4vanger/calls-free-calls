window.app.controller 'PanelCtrl', [
	'$scope'
	'adProviders'
	($scope, adProviders) ->
		$scope.list = adProviders.list()
		$scope.$watchCollection(
			(-> adProvider.isShown for adProvider in $scope.list)
			(newVisible, oldVisible) ->
				expandedIndex = (index for visible, index in newVisible when visible && !oldVisible[index])
				return if expandedIndex.length != 1
				expandedIndex = expandedIndex[0]
				# hide all other
				adProvider.isShown = false for adProvider, index in $scope.list when index != expandedIndex
		)
]
