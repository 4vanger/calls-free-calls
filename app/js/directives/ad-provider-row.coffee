window.app.directive 'adProviderRow', ['$compile', '$animate', ($compile, $animate) ->
	restrict: 'A'
	replace: true
	templateUrl: 'ad-provider-row.tpl.html'
	scope:
		adProvider: '=adProviderRow'
	link : (scope, elem) ->
		# convert adProvider name into directive-like name
		directiveName = scope.adProvider.type.replace /[A-Z]/g, (match) -> '-' + match.toLowerCase()
		deregister = scope.$watch 'adProvider.isShown', (isShown) ->
			return unless isShown
			# dynamically insert directive name depending on adProvider type
			content = $compile('<div ' + directiveName + '></div>')(scope);
			container = elem.find('div')
			container.append(content)
			# we need to add element only once - deregister listener after
			deregister()
]
