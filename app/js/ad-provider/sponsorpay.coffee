angular.module('sponsorPay', ['adProviders'])
.run ['adProviders', (adProviders) ->
	adProviders.register {
		type: 'sponsorPay'
		title: 'SponsorPay'
		logo: 'img/sponsorpay.png'
	}
]

.directive 'sponsorPay', ->
	restrict: 'AC'
	replace: true
	scope: true
	templateUrl: 'sponsorpay.tpl.html'
	link: (scope, el) ->
		scope.isLoaded = false
		iframe = el.find('iframe')[0]
		iframe.onload = iframe.onerror = -> scope.$apply('isLoaded = true')
