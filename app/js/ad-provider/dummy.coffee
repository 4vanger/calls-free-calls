angular.module('dummy', ['adProviders'])
.run ['adProviders', (adProviders) ->
	adProviders.register {
		type: 'dummy'
		title: 'Dummy advertiser'
		logo: 'img/dummy.png'
	}
	adProviders.register {
		type: 'dummy'
		title: 'Dummy advertiser'
		logo: 'img/dummy.png'
	}
	adProviders.register {
		type: 'dummy'
		title: 'Dummy advertiser'
		logo: 'img/dummy.png'
	}
	adProviders.register {
		type: 'dummy'
		title: 'Dummy advertiser'
		logo: 'img/dummy.png'
	}
	adProviders.register {
		type: 'dummy'
		title: 'Dummy advertiser'
		logo: 'img/dummy.png'
	}
]

.directive 'dummy', ['$timeout', ($timeout) ->
	restrict: 'AC'
	replace: true
	scope: true
	templateUrl: 'dummy.tpl.html'
	link: (scope, el) ->
		scope.isLoaded = false
		$timeout (-> scope.isLoaded = true), 1000
]
