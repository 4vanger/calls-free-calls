angular.module('adProviders', [])
.factory 'adProviders', ->
	providers = []
	map = {}
	register: (args) ->
		return unless args.type
		providers.push args
		map[args.type] = args
	list: -> providers
	get: (name) -> map[name]

window.app = angular.module('callsFreeCalls', ['ngRoute', 'ngAnimate', 'partials', 'adProviders', 'sponsorPay', 'dummy'])
.config [
	'$routeProvider'
	'$locationProvider'
	($routeProvider, $locationProvider) ->
		$locationProvider.hashPrefix('!')
		# Without server side support html5 must be disabled.
		$locationProvider.html5Mode(false)

		$routeProvider
			.when '/',
				templateUrl: 'panel.tpl.html'
				controller: 'PanelCtrl'
]

