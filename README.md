### Test task for calls-free-calls

### Installation
You need to have node.js (http://nodejs.org/) installed.
You will need to install Brunch
> npm install -g brunch

Install everything for a project:
> npm install
> brunch install

Run a project:
> npm start

Open your browser and point it to http://localhost:3000/
